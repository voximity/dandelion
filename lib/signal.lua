local signal = {}

signal.listeners = {}
signal.on = function(name, callback)
	local listener = {name = name, callback = callback}
	table.insert(signal.listeners, listener)
	return listener
end
thread.spawn(function()
	while true do
		local name, args = signal.pull(nil, 1)
		if name then
			for i,v in next, signal.listeners do
				if v.name == name then
					v.callback(args)
				end
			end
		end
		thread.suspend()
	end
end)

signal.push = function(name, ...)
	computer.pushSignal(name, ...)
end

signal.pull = function(name, timeout)
	local start = computer.uptime()

	local s, args
	repeat
		data = {computer.pullSignal(timeout)}
		s = data[1]
		args = {}
		for i = 2, #data do args[i - 1] = data[i] end
	until (name and s == name) or (not name) or computer.uptime() > timeout + start

	return s, args
end

return signal