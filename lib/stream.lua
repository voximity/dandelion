local stream = {}

stream.new = function(onWrite)
	local s = setmetatable({}, {__index = stream})
	s.data = ""
	s.flushed = ""
	s.reading = false
	s.onWrite = onWrite

	return s
end

function stream:write(data)
	self.data = self.data .. data
	self.onWrite(data)
end
function stream:splice(s, e)
	self.data = self.data:sub(s, e)
end

function stream:flush()
	local data = self.data
	self.data = ""
	self.reading = false
	self.flushed = data
	return data
end

function stream:read()
	self.reading = true
	repeat
		os.sleep()
	until not self.reading
	return self.flushed
end

return stream