local keyboard = {}

keyboard.keys = {
	backspace       = 0x0E,
	delete          = 0xD3,
	down            = 0xD0,
	enter           = 0x1C,
	home            = 0xC7,
	lcontrol        = 0x1D,
	left            = 0xCB,
	lalt            = 0x38,
	lshift          = 0x2A,
	pageDown        = 0xD1,
	rcontrol        = 0x9D,
	right           = 0xCD,
	ralt            = 0xB8,
	rshift          = 0x36,
	space           = 0x39,
	tab             = 0x0F,
	up              = 0xC8,
	["end"]         = 0xCF,
	enter           = 0x1C,
	tab             = 0x0F,
	numpadenter     = 0x9C,
}



return keyboard