function loadfile(path)
	local bootAddress = computer.getBootAddress()
	local file, fileOpenError = component.invoke(bootAddress, "open", path)
	assert(file, fileOpenError)
	local buffer = ""
	local data, dataError
	repeat
		data, dataError = component.invoke(bootAddress, "read", file, math.huge)
		buffer = buffer .. (data or "")
	until not data
	component.invoke(bootAddress, "close", file)
	local s,e = load(buffer, "=" .. path, "bt", _G)
	if not s then error(e) end
	return s()
end

do
	local loaded = {}
	function require(name)
		if loaded[name] then return loaded[name] end
		loaded[name] = loadfile("/lib/" .. name .. ".lua")
		return loaded[name]
	end
end

do loadfile("/system/boot/boot.lua") end

thread = loadfile("/system/boot/thread.lua")
os = loadfile("/system/os.lua")
term = loadfile("/system/term.lua")(component.gpu)

thread.spawn(function()
	while true do
		term.writeLine("Every second!")
		thread.sleep(1)
	end
end)
thread.spawn(function()
	require("signal").on("key_down", function(_, addr, ch, co)
		term.writeLine(addr)
	end)
end)

thread._scheduler()