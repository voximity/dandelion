-- Provided to app executable files is an "app" table which contains context information and
-- ways to interact with the computer.

-- Additionally, all libraries in system/ are included naturally.

local content = app.args["content"] or "Hello, world!"
app.writeLine(content .. "\n")