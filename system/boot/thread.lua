local thread = {}

thread.threads = {}

function thread.push(th)
	table.insert(thread.threads, th)
	return th
end

function thread.pushBack(th)
	table.insert(thread.threads, 1, th)
	return th
end

function thread.pop()
	return table.remove(thread.threads)
end

function thread._scheduler()
	while true do
		if #thread.threads == 0 then os.sleep() end
		local t = thread.pop()

		local time = computer.uptime()
		local ret

		if t.state == "resume" then
			t.state = "running"
			if not t.args then t.args = {} end
			ret = {coroutine.resume(t.co, table.unpack(t.args))}
		elseif t.state == "wait" then
			if time > t.suspend_time + t.delay then
				t.state = "running"
				ret = {coroutine.resume(t.co, time - t.suspend_time)}
				t.suspend_time = nil
				t.delay = nil
			else
				thread.pushBack(t)
			end
		elseif t.state == "yield" then
			local yieldRet = {t.f(table.unpack(t.args))}
			if table.remove(yieldRet, 1) then
				t.state = "running"
				ret = {coroutine.resume(t.co, table.unpack(yieldRet))}
			else
				thread.pushBack(t)
			end
		end

		if ret and not ret[1] then
			error(ret[2])
		end

		os.sleep()
	end
end

function thread.run(f, ...)
	thread.push({state = "resume", co = coroutine.create(f), is_coroutine = false, args = {...}})
	coroutine.yield()
end

function thread.spawn(f, ...)
	thread.pushBack({state = "resume", co = coroutine.create(f), is_coroutine = false, args = {...}})
end

function thread.sleep(delay)
	checkArg(1, delay, "number")
	thread.pushBack({state = "wait", co = coroutine.running(), suspend_time = computer.uptime(), delay = delay})
	return coroutine.yield()
end

function thread.suspend()
	thread.sleep(-1)
end

function thread.yield(f, ...)
	checkArg(1, f, "function")
	thread.pushBack({state = "yield", co = coroutine.running(), f = f, args = {...}})
	return coroutine.yield()
end

return thread