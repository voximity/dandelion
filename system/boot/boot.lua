local invoke = component.invoke

local gpu = component.list("gpu")()
local screen = component.list("screen")()
assert(gpu and screen, "No valid GPU or screen found!")

gpu = component.proxy(gpu)
local w, h = gpu.getResolution()
gpu.bind(screen)

gpu.setForeground(0xFFFFFF)
gpu.setBackground(0x000000)
gpu.fill(1, 1, w, h, " ")

component.gpu = gpu