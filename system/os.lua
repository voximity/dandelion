local os = {}

do
	local signal = require("signal")
	os.sleep = function(timeout)
		local start = computer.uptime()
		repeat
			signal.pull(nil, timeout or 0.01)
		until not timeout or computer.uptime() >= start + timeout
	end
end

return os