local term = {}

term.gpu = nil
term.x = 1
term.y = 1

term.setForeground = function(c) term.gpu.setForeground(c) end
term.setBackground = function(c) term.gpu.setBackground(c) end
term.setColor = function(f, b) term.setForeground(f) term.setBackground(b) end

term.clear = function()
	local w, h = term.getResolution()
	term.gpu.fill(1, 1, w, h, " ")
end

term.reset = function()
	term.setColor(0xFFFFFF, 0x000000)
	term.clear()
end

term.write = function(text)
	local text = tostring(text)
	local w, h = term.gpu.getResolution()

	for i = 1, #text do
		local token = text:sub(i, i)
		if token == "\n" then
			term.x = 1
			term.y = term.y + 1
		else
			term.gpu.set(term.x, term.y, token)
			term.x = term.x + 1
		end

		if term.x > w then
			term.x = 1
			term.y = term.y + 1
		end
		if term.y > h then
			term.gpu.copy(1, 2, w, h - 1, 0, -1)
			term.gpu.fill(1, h, w, 1, " ")
			term.y = h
		end
	end
end

term.writeLine = function(text)
	term.write(tostring(text) .. "\n")
end

term.setCursor = function(x, y)
	local w, h = term.gpu.getResolution()

	term.x = math.min(x, w)
	term.y = math.min(y, h)
end

return function(gpu) term.gpu = gpu return term end