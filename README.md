# dandelion

Dandelion is a completely custom, non-OpenOS compatible operating system for OpenComputers. It features a tile-based UI with terminals.

**This is a huge WIP.** It will not function alone yet. I am still writing the fundamental backend.

## Compatibility

Dandelion, at the time of writing, is not compatible with any OpenOS executables. Dandelion uses a proprietary app type and redefines almost every method used by OpenOS.

## Installation

There is no way to install Dandelion currently besides putting the master branch in the HDD in your world save. Installer is coming soon.